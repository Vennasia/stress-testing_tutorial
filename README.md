## 压力测试总览
![](assets/压力测试.png)

压力测试除了能测试性能外,还能顺带检测两种错误类型:
+ 内存泄漏
+ 并发与同步

要想压力测试是可信的:
+ 重复
+ 并发
+ 量级
+ 随机变化

常见的工具有:
+ jMeter
+ Locus
+ Gatling

## 性能指标
![](assets/性能指标.png)

+ 吞吐量
    + qps(Query per Second)
    + tps(Transaction per Second)
+ 响应时间
    + max(Max ResponseTime)
    + min(Minimum ResponseTime)
    + 90%响应时间(90%Response Time)
+ 错误率

## jMeter
### 下载
https://jmeter.apache.org/download_jmeter.cgi
### 使用
### 创建测试组
模拟n个人同时请求某个接口

![](assets/JMeter-线程组.png)
### 采样器
![](assets/JMeter-采样器.png)

取样器决定了你测试的是什么

比如要测http请求,就用http采样器
### 监听器 之 查看结果树
![](assets/JMeter-监听器-查看结果树.png)
能看到每一次请求是成功了还是失败了,以及成功了响应结果是什么
### 监听器 之 汇总报告
![](assets/JMeter-监听器-汇总报告.png)
### 监听器 之 聚合报告
![](assets/JMeter-监听器-聚合报告.png)
### 监听器 之 汇总图
![](assets/JMeter-监听器-汇总图.png)
### 开始运行
![](assets/JMeter-开始运行.png)
### JMeter在windows下地址占用bug解决
>https://support.microsoft.com/en-us/help/196271/when-you-try-to-connect-from-tcp-ports-greater-than-5000-you-receive-t
![](assets/性能压测-压力测试-JMeter在windows下地址占用bug解决.png)

## 优化方向
![](assets/优化方向.png)

## 性能压测-优化-中间件对性能的影响
### nginx
```
docket stats
```

![](assets/单压nginx.png)

nginx主要比较浪费cpu,内存使用并不多(百分之一二的样子)

这是由于 nginx直接是将所有的东西交给别人,别人去处理

它自己不需要创建对象也不需要用多大的内存,只用把别人返回的结果再返回就是了

它更多需要的是拥有更多的线程,来接受我们更多的请求,来进行处理就行了

所以我们cpu呢要来回在线程之间切换计算,所以我们浪费的cpu比较多
### spring-cloud-starter-gateway
情况基本同nginx 都是cpu密集型

对内存要求很小(下面我们设置的最大内存才75mb,但都搓搓有余了)

![](assets/单压spring-cloud-gateway.png)

![](assets/单压spring-cloud-gateway2.png)
### 简单服务
访问`/hello` 返回hello字符串
![](assets/单压简单服务.png)

![](assets/单压简单服务2.png)
### Gateway+简单服务(主要是网络io开销)
![](assets/Gateway+简单服务.png)

![](assets/Gateway+简单服务2.png)
### Nginx+Gateway+简单服务
![](assets/Nginx+Gateway+简单服务.png)

## 性能压测-优化-吞吐量
### 首页展示(db,thymeleaf,静态资源)
![](assets/单压商品微服务-首页展示.png)

![](assets/单压商品微服务-首页展示2.png)
主要被db和thymeleaf拖累,另外频繁gc也是个问题

![](assets/单压商品微服务-首页展示3.png)
此时我们让请求首页时也要把资源请求回来,发现吞吐量更小了,这是因为首页对应的tomcat服务器线程资源争抢严重的问题
#### nginx动静分离
![](assets/Nginx动静分离.png)

![](assets/Nginx动静分离2.png)

![](assets/Nginx动静分离3.png)
#### 数据库索引 与 微服务数据库日志相关打印级别
![](assets/数据库索引和微服务数据库相关日志.png)

### 三级分类数据获取(循环中多次db查询)
![](assets/单压商品微服务-三级分类.png)

![](assets/单压商品微服务-三级分类2.png)

主要原因是循环中多次db查询,减少查询次数即可

![](assets/单压商品微服务-三级分类3.png)

再则就是使用内存数据库了 比如redis(存在内存里的数据比本地快的多)

![](assets/redis缓存(内存)数据库.png)

## 性能压测-优化-模拟线上应用内存崩溃宕机情况
如果一个服务的jvm内存设置过小 当并发很高的时 就很容易因为内存不够而宕机
![](assets/性能压测-优化-模拟线上应用内存崩溃宕机情况.png)

![](assets/性能压测-优化-模拟线上应用内存崩溃宕机情况2.png)

由100m->1000m

扩大内存也能稍微提高吞吐量(gc时间减少)
![](assets/性能压测-优化-模拟线上应用内存崩溃宕机情况3.png)

## 压测过程中常见HTTP请求错误
### socket closed

![](assets/常见HTTP请求错误.png)

### 压测时 spring-boot-starter-data-redis中lettuce导致 OutOfDirectMemoryError



OutOfDirectMemoryError (direct memory 直接内存 即堆外内存 直接操作内存条的)

springboot2.0以后默认使用lettuce作为操作redis的客户端

它使用netty进行网络通讯,但是有bug

lettuce底层操作netty时会导致堆外内存溢出 (资源没有即使释放所导致)

netty如果没有指定堆外内存,会默认使用我们指定的 -Xmx300m (VM options)

但我们其实应该通过设置 Dio.netty.maxDirectMemory 来调大堆外内存

但仅仅是调大堆外内存 只会延缓错误的发生

解决方案:
+ 法一)、 升级lettuce客户端
+ 法二)、 切换使用jedis (比较老,没更新了,lettuce性能更高(使用了netty))


![](assets/spring-boot-starter-data-redis中lettuce导致 OutOfDirectMemoryError.png)
